# Test Laravel

## API csv_read


### POST /api/contacts/upload_csv
#### payload:
    csv_file: csv file.
    response
    [
    {
    "CSV_columns": (array),
    "table_columns": (array),
    "file_name": (file name)
    }
    ]

### POST /api/contacts/save_csv
#### payload
    file_name:(file_name)
    field_relations:(relation field array)
    customs:(custom fields array)

#### example:
    file_name:1600097829_00archivo.txt
    field_relations:[{"table":"team_id","csv":"team"},{"table":"phone","csv":"otro"},{"table":"name","csv":"algo"},{"table":"email","csv":"nuevo"}]
    customs:["e","g"]